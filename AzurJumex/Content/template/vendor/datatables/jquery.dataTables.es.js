﻿var language = {
    lengthMenu: "Mostrar _MENU_ registros por página",
    zeroRecords: "Ningun dato encontrado",
    info: "Mostrando página _PAGE_ de _PAGES_",
    infoEmpty: "Ningun registro disponible",
    loadingRecords: "Cargando...",
    processing: "Procesando...",
    search: "Buscar:",
    paginate: {
        "first": "Primero",
        "last": "Último",
        "next": "Siguiente",
        "previous": "Atras"
    },
    infoFiltered: "(Filtrado de _MAX_ registro total)"
};
